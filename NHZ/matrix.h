/**
 * @file matrix.h
 * @author Molnár-Barabás Ákos
 * @date 30 Nov 2019
 * @brief Mátrix könyvtár header dokumentáció.
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <math.h>


/**
 * @brief Mátrix típusu változó értelmezése
 *
 *  Ebben a részben van értelmezve maga a mátrix "objektum" amit a függvények használnak és amit a
 *  felhasználó is használhat, értelmezhet vele mártix változókat. Ez a struktúra azért könyebb, mivel
 *  megkönnyyti a márixokkal való bajlódást, a mérete mindig elérhető lesz a felhasználó számára és
 *  így könyebb az alprogramokat is használni, viszont ezeket TILOS szerkeszteni, mivel memory leak-hez
 *  vezethet.
 *  Továbbá egy észbentartandó dolog, hogy, mivel a strukúra a mátrix értékeit double-ben térolja, ezért
 *  felléphet egy olyan jelenség, hogy az értékek nem lesznek pontosak, ezért az értékellenörzést nem tudjuk
 *  egy "==" operátórral elvégezni, csak azt tudjuk megnézni, hogy egy elem 0-e.
 */
typedef struct {
    double** value;             /**< Ez a változó tartalmazza a mátrix típusu változó elemeit, ez a vátozó egy két dimenziós tömb. */
    int row_size, col_size;     /**< Ezek a változók tartalmazzák a mátrix dimenzióit, ezeknek nem ajánlott az átírása, mivel az írt függvények függenek ezektől.*/
}matrix;

/**
 * @brief A lehetséges error kódok enumerációja
 *
 *  Minden egyes függvény egy error kódot térít vissza, ezzel jelezve a függvény futásának eredményét.
 */
typedef enum{
    SUCCESS,                /**< Sikeres futás hibakód, akkor kapjuk vissza ha a művelet sikeresen lefutott. */
    OUT_OF_MEMORY,          /**< Memória kifutási hibakód, akkor kapjuk vissza, ha nem sikerült foglalni memóriát. */
    INCOMPATIBLE_TYPES,     /**< Inkompatibilis méretek hibakód, akkor kapjul vissza, ha bizonyos műveleteknél nem megfelelő a mátrixok dimenziója. */
    FILE_NOT_FOUND,         /**< Fájl nem kapható hibakód, akkor kapjuk vissza, ha az elérni kívánt file nem elérhtő. */
    NULL_POINTER,           /**< Null pointer hibakód, akkor kapjuk vissza, ha nem sikerült foglalni, vagy a műveletet nem lehet végrehajtani, mivel a kapott mátrix hibás. */
    CANNOT_DELETE_POINTER   /**< Pointer nem törölhető, akkor kapjuk vissza, ha a pointert nem lehet törölni, valami hiba folytán. */
}err_code;

/**
 * @brief Matrix initialize
 *
 *  Ez az alprogram felelős a mátrix változó incializálásáért. Memóriában dinamikusan foglal helyet a mátrix számára.
 *  Használata kötelező minden egyett mtxdel() függvényhívás után, illetve minden felvett vátozó esetén, abban az esetben,
 *  ha ismét szeretnénk használni a változót, egyébb fügvényeknek szeretnénk őt átadni. Amennyiben nem hívjuk meg az
 *  init függvényt meg fog hívódni a felszabadídás függvény egy nem lefoglalt memóriarészre.
 *
 *  FONTOS: Amennyiben az inicializáló függvényt nem egy újonnan felvett, vagy törölt mátrix változóra hívjuk meg,
 *  akkor memory leak fog történni, elvész az előzőleg foglalt adat.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix pointerét.
 * @param size_of_row Ebben a változóban adjuk át a függvénynek a mátrix sorainak méretét.
 * @param size_of_column Ebben a változóban adjuk át a függvénynek a  mátrix oszlopainak méretét.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért,
 * egyedül azt nem tudja leellenőrizni, hogy le van-e benne foglalva valami.
 */
err_code mtxinit(matrix *matrix_ptr, int size_of_row ,int size_of_column);

/**
 * @brief Matrix zero initialize
 *
 *  Ez az alprogram felelős a mátrix változó incializálásáért és a mátrix 0-val való feltöltéséért. Ugyanazok a szabályok
 *  vonatkoznak rá, mint a sima inicializálás függvényre, meghívás és használat terén.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix pointerét.
 * @param size_of_row Ebben a változóban adjuk át a függvénynek a mátrix sorainak méretét.
 * @param size_of_column Ebben a változóban adjuk át a függvénynek a  mátrix oszlopainak méretét.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért,
 * egyedül azt nem tudja leellenőrizni, hogy le van-e benne foglalva valami.
 */
err_code mtxzinit(matrix *matrix_ptr, int size_of_row, int size_of_column);

/**
 * @brief Matrix equal to
 *
 *  Ez az alprogarm lehetővé teszi, hogy létrehozzunk egy egyenlő értékű de független mátrix változót,
 * mivel ha két mátrixot egyenlővé teszünk akkor egyrészt memory leak lesz és ha valamelyik mátrixot
 * változtatjuk az egyenlővé tettek közül a másik is változni fog. Ez a mátrix struktúra tulajdonságából
 * adódik, mivel amikor "=" értékadást adunk két mátrx között, az értékre mutató pointer felül fog íródni
 * a másik mátrix pointerével, így az elérhetetlenné válik a kód további részében. Továbba a két pointer
 * ugyanazokra az értékekre mutatat, így változatatható lesz az érték mindkét mutatón keresztül.
 *  Az alprogram által végzett művelet megfelel az "=" értékadásnak, tehát az első átküldött mátrix értéke
 * törölve lesz(a többi "=" értékadással vele egyenlővé tett mátrixok értékével együtt) és kap egy új,
 * a második mátrixal egyenlő értéket. Ha a mátrixok eltérő méretüek a méretük egyeztetve lesz.
 *
 * @param matrix_ptr1 Ebben a változóban adjuk át a függvénynek az egyik mátrix pointerét.
 * @param matrix_ptr2 Ebben a változóban adjuk át a függvénynek a másik mátrix pointerét.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxeqto(matrix* matrix_ptr1, matrix* matrix_ptr2);

/**
 *@brief Matrix inverz
 *
 * Ez a függvény felelős az inverzmátrix számításáért.
 *
 *
 * @param matrix_ptr1 Ebben a változóban adjuk át az invertálásra szoruló függvényt.
 * @param matrix_inv_result Ebben a változóban fog tárolódni a művelet eredménye, muszáj inicializálni, de nem muszáj törölni átadás elött.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxinv(matrix *matrix_ptr1, matrix *matrix_inv_result);

/**
 * @brief Matrix multilpy
 *
 *  Ez az alprogram felelős két mátrix szorzásáért.
 *
 * @param matrix_ptr1 Ebben a változóban adjuk át a mátrix szorzás bal oldali elemét.
 * @param matrix_ptr2 Ebben a változóban adjuk át a mátrix szorzás jobb oldali elemét.
 * @param mtxmult_result Ebben a változóban fog tárolódni a művelet eredménye, muszáj inicializálni, de nem muszáj törölni átadás elött.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxmult(matrix *matrix_ptr1, matrix *matrix_ptr2, matrix *mtxmult_result);

/**
 * @brief Matrix multilpy
 *
 * Ez az alprogram felelős egy mátrix skalárral való szorzásáért.
 *
 * @param matrix_ptr1 Ebben a változóban adjuk át a mátrixot amit skallárral szeretnénk szorozni.
 * @param number Ebben a változóban adjuk át a skalárt amivel szorozzuk a mátrixot.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxsmult(matrix *matrix_ptr1, double number);

/**
 * Matrix addition
 *
 * Mátrix összeadás művelet ugyan nem létezik a matematikában, vaamiért mégis megigértem a specifikációban.
 * Ez a függvény összeadja a mátrix1[i][j] elemét a mátrix2[i][j] elemével és az eredményt berakja az eredménymétrix
 * matrixadd_result[i][j]-be, természeresen mindkét összeadandó mátrix [m]x[n]-es méretű kell legyen.
 *
 * @param matrix1_ptr1  Az egyik összeadandó mátrix.
 * @param matrix_ptr2   A másik összeadandó mátrix.
 * @param matrixadd_resoult Az eredménymátrix.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxadd(matrix *matrix1_ptr1, matrix *matrix_ptr2, matrix *matrixadd_result);

/**
 * @brief Gauss elimination
 *
 * Ez a függvény felelős a Gauss-eliminációért.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrixot amit gausseliminálni szeretnénk.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code gelim(matrix* matrix_ptr);

/**
 *  @brief Matrix determinant calculate
 *
 *  Ez a függvény felelős a mátrix determinánsának kiszámolásáért.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix pointerjét.
 * @param det Ebben a változóban fog tárolódni a determináns értéke.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxdcalc(matrix* matrix_ptr, double *det);

/**
 * @brief Matrix read file
 *
 *  Ez a függvény felelős a mátrix kiolvasásáért fájlból, kiterjesztés független, olyan kiterjesztésű fájlból olvas,
 *  amilyenből csak szeretnénk.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix pointerjét.
 * @param Filename Ebben a változóban adjuk át a függvénynek a mátrix fájljának nevét.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxrf(matrix *matrix_ptr, char *Filename);

/**
 * @brief Matrix write file
 *
 * Ez a függvény felelős a mátrix fájlba írásáért, kiterjesztés független, olyan kiterjesztésű fájlba ír,
 * amilyenbe csak szeretnénk.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix értékét.
 * @param Filename Ebben a változóban adjuk át a függvénynek a mátrix fájljának nevét.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxwf(matrix matrix_ptr, char *Filename);

/**
 * @brief Matrix print
 *
 * Ey a függvény felelős a mátrix konzolos kiiratásáért.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix értékét.
 */
err_code mtxprint(matrix matrix_ptr);

/**
 * @brief Matrix delete
 *
 * Ez a függvény felelős a mátrix dinamikusan foglalt értékeinek felszabaditásáért, figyelmesen kell használni, amennyiben
 * nem használjuk egy program végén a mátrix változók törléséhez, akkor memóriaszivárgás történhet.
 *
 * @param matrix_ptr Ebben a változóban adjuk át a függvénynek a mátrix pointerjét.
 * @return Ez a függvény egy error code-ot térít vissza, annak megfelelően, hogy sikerült-e a művelet, és ha nem akkor miért.
 */
err_code mtxdel(matrix *matrix_ptr);

#endif //MATRIX_H