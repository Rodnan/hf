#include <stdio.h>
#include "matrix.h"
#include "debugmalloc.h"

///Ez a kód demonstráció képpen készűlt, szemlélteti, hogy hogyan működik a mátrix függvénykönyvtár

int main() {


    matrix M, A;
    printf("\n\n===================A es M init====================\n");
    printf("%s", mtxinit(&M, 5, 5) == SUCCESS? "Success\n":"Fail\n");
    for(int i = 0; i < M.row_size; i++){
        for(int j = 0; j < M.col_size; j++){
            M.value[i][j] = i+j+2;            ///A mátrixokat manuálisan is feltölthetjük
        }
    }


    printf("%d\n",mtxzinit(&A, 5, 5)); /// Az A mátrix értelmezése és feltöltése 0-val.
    A.value[0][0] = 2;
    A.value[1][1] = 6;
    A.value[2][2] = 1;
    A.value[3][3] = 5;
    A.value[4][4] =2;
    printf("M[2][2]: %lf \nA[2][1]: %lf \n", M.value[2][2], A.value[2][1]); /// A mátrixok értékei elérhetőek.

    printf("\n\n================M es A ertekadas utan================");  /// A és M kiiratása az értékadás után.
    printf("\nM:\n");
    mtxprint(M);
    printf("\nA:\n");
    mtxprint(A);

    printf("\n\n==================matrixszorzas===================\n"); /// Mátrixok szorzása
    matrix B;
    mtxinit(&B, 1, 1);
    printf("A szorzas hibakodja: %d\n",mtxmult(&A, &M, &B));
    printf("\nB:");
    mtxprint(B);

    printf("\n\n===================file kezeles=====================");
    mtxwf(M, "matrix1");
    printf("\nM:");
    mtxprint(M);
    mtxrf(&A, "matrix1");
    printf("\nA:");
    mtxprint(A);

    printf("\n\n==================Modositas====================");
    mtxeqto(&A, &B);                    ///Ha csak az "=" operátort használjuk akkor a következő sorban történő értékadás
    B.value[1][1] = 420.69;              ///során az A[1][1] és B[1][1] is 420.69 lesz
    printf("\nA:");
    mtxprint(A);
    printf("\nB:");
    mtxprint(B);

    printf("\n\n======================Gauss=========================");
    printf("\nA:");
    mtxprint(A);
    gelim(&A);
    printf("\nGauss(A):");
    mtxprint(A);

    mtxdel(&M);
    mtxdel(&A);
    mtxdel(&B);


    printf("\n===================determinans======================\n");
    mtxinit(&B, 1, 1);       ///Törlés utáni újrainicializálás
    mtxrf(&B, "matrix");
    double d;
    mtxdcalc(&B, &d);
    mtxprint(B);
    printf("det(B) = %lf\n", d);


    printf("\n===================inverz matrix======================\n");
    mtxinit(&A, 1, 1);
    mtxinv(&B,&A);
    printf("B inverze:");
    mtxprint(A);
    printf("\nA*B:");
    mtxinit(&M, 1,1);
    mtxmult(&A, &B, &M);                        ///inverz tesztelése A*B
    mtxprint(M);
    mtxdel(&M);

    printf("\nB*A:");
    mtxinit(&M, 1,1);
    mtxmult(&B, &A, &M);                        ///inverz tesztelése B*A
    mtxprint(M);

    mtxsmult(&M, 10000000000);           ///A pontatlan számolás demonstrálása a double tuldjdonságából kiindulva
    if(M.value[3][4] == 0){
        printf("\nIgaz, hogy M[3][3] = 0.\n");     ///A 0 leszögezett értékének demonstálása
    }
    printf("\nM*10000000000:");
    mtxprint(M);

    printf("\n=================matrix osszeadas===================\n");
    mtxdel(&M);
    printf("\nA:");
    mtxprint(A);
    printf("\nB:");
    mtxprint(B);
    mtxinit(&M, 1, 1);
    mtxadd(&A, &B, &M);
    printf("\nA+B:");
    mtxprint(M);

    mtxdel(&M);
    mtxdel(&A);
    mtxdel(&B);
    return 0;
}