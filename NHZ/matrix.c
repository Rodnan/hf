#include "matrix.h"
#include "debugmalloc.h"

static bool null_pointer_check(const matrix* matrix1);
static err_code Gauss(matrix *matrix1, double *coef);
static void mtxinv_init(matrix *matrix1,matrix *matrix2);
static bool inv_det_check(matrix matrix1);
static void mtxinv_sep(matrix *matrix1, matrix *matrix2);
static void aprox(matrix *matrix1);


err_code mtxinit(matrix *matrix1, int const row_size, int const col_size){

    matrix1 -> row_size = row_size;
    matrix1 -> col_size = col_size;

    matrix1 ->value = (double**)malloc(row_size* sizeof(double*));
    if(matrix1->value == NULL)
        return OUT_OF_MEMORY;

    for(int i = 0; i < row_size; i++){
        matrix1 -> value[i] = (double*)malloc(col_size* sizeof(double));
        if (matrix1->value[i] == NULL)
            return OUT_OF_MEMORY;
    }

    return SUCCESS;
}


err_code mtxzinit(matrix *matrix1, int const row_size, int const col_size){

    matrix1->row_size = row_size;
    matrix1->col_size = col_size;

    matrix1 ->value = (double**)calloc(row_size, sizeof(double*));
    if(matrix1->value == NULL)
        return OUT_OF_MEMORY;

    for(int i = 0; i < row_size; i++){
        matrix1 -> value[i] = (double*)calloc(col_size, sizeof(double));
        if (matrix1->value[i] == NULL)
            return OUT_OF_MEMORY;
    }

    return SUCCESS;
}


err_code mtxeqto(matrix* matrix1, matrix* matrix2){

    err_code state = mtxdel(matrix1);

    if(state != SUCCESS){
        if(state == NULL_POINTER)
            return NULL_POINTER;
        return CANNOT_DELETE_POINTER;
    }

    if(mtxinit(matrix1, matrix2->row_size, matrix2->col_size) == OUT_OF_MEMORY)
        return OUT_OF_MEMORY;

    for (int i = 0; i < matrix1->row_size; ++i) {
        for (int j = 0; j < matrix1->col_size; ++j) {
            matrix1->value[i][j] = matrix2->value[i][j];
        }
    }

    return SUCCESS;
}


err_code mtxinv(matrix *matrix1, matrix *matrix2){

    if(null_pointer_check(matrix1))
        return NULL_POINTER;

    if(matrix1->col_size != matrix1->row_size)
        return INCOMPATIBLE_TYPES;

    matrix temp;
    mtxinv_init(matrix1, &temp);
    gelim(&temp);
    if(inv_det_check(temp)){
        mtxdel(matrix2);
        mtxinit(matrix2,matrix1->row_size,matrix1->col_size);
        mtxinv_sep(&temp, matrix2);
        mtxdel(&temp);
        return SUCCESS;
    }
    mtxdel(matrix2);
    mtxdel(&temp);
    mtxzinit(matrix2,1,1);
    return INCOMPATIBLE_TYPES;
}


err_code mtxmult(matrix *matrix1, matrix *matrix2, matrix *matrix3){

    if(null_pointer_check(matrix1) || null_pointer_check(matrix2))
        return NULL_POINTER;

    if(matrix1->row_size != matrix2->col_size){
        return INCOMPATIBLE_TYPES;
    }

    err_code state = mtxdel(matrix3);

    if(state != SUCCESS){
        if(state == NULL_POINTER)
            return NULL_POINTER;
        return CANNOT_DELETE_POINTER;
    }

    if(mtxzinit(matrix3, matrix2->row_size, matrix2->col_size) == OUT_OF_MEMORY)
        return OUT_OF_MEMORY;

    for(int i = 0; i < matrix3->row_size; i++){
        for (int j = 0; j < matrix3->col_size; j++) {
            for(int k = 0; k < matrix1->row_size; k++)
                matrix3->value[i][j] += matrix1->value[i][k] * matrix2->value[k][j];
        }
    }

    aprox(matrix3);
    return SUCCESS;
}


err_code mtxsmult(matrix *matrix1, double const n){

    if(null_pointer_check(matrix1))
        return NULL_POINTER;

    for (int i = 0; i < matrix1->row_size; ++i) {
        for (int j = 0; j < matrix1->col_size; ++j) {
            matrix1->value[i][j] *= n;
        }
    }

    aprox(matrix1);

    return SUCCESS;
}


err_code mtxadd(matrix *matrix1, matrix *matrix2, matrix *matrix3){
    if(null_pointer_check(matrix1)||null_pointer_check(matrix2))
        return NULL_POINTER;

    if((matrix1->row_size != matrix2->row_size) || (matrix1->col_size != matrix2->col_size)){
        return INCOMPATIBLE_TYPES;
    }

    err_code state = mtxdel(matrix3);

    if(state != SUCCESS){
        if(state == NULL_POINTER)
            return NULL_POINTER;
        return CANNOT_DELETE_POINTER;
    }

    if(mtxzinit(matrix3, matrix2->row_size, matrix2->col_size) == OUT_OF_MEMORY)
        return OUT_OF_MEMORY;

    for(int i = 0; i < matrix3->row_size; i++){
        for (int j = 0; j < matrix3->col_size; j++) {
            matrix3->value[i][j] += matrix1->value[i][j] + matrix2->value[i][j];
        }
    }
    return SUCCESS;
}


err_code gelim(matrix *matrix1){
    double temp;
    return Gauss(matrix1, &temp);
}


err_code mtxdcalc(matrix *matrix1, double *det){

    *det = 0;
    err_code state;

    if(null_pointer_check(matrix1))
        return NULL_POINTER;

    if(matrix1->row_size != matrix1->col_size){
        return INCOMPATIBLE_TYPES;
    }

    if(matrix1->row_size ==2){
        *det = matrix1->value[0][0]*matrix1->value[1][1] - matrix1->value[0][1]*matrix1->value[1][0];
    }
    else if(matrix1->row_size == 3){
        *det = matrix1->value[0][0]*matrix1->value[1][1]*matrix1->value[2][2] + matrix1->value[1][0]*matrix1->value[2][1]*matrix1->value[0][2] + matrix1->value[2][0]*matrix1->value[0][1]*matrix1->value[1][2] - matrix1->value[0][2]*matrix1->value[1][1]*matrix1->value[2][0] - matrix1->value[1][2]*matrix1->value[2][1]*matrix1->value[0][0] - matrix1->value[2][2]*matrix1->value[0][1]*matrix1->value[1][0];
    }
    else{
        matrix temp;
        mtxinit(&temp, 1, 1);
        mtxeqto(&temp, matrix1);
        state = Gauss(&temp, det);

        if(state != SUCCESS){
            mtxdel(&temp);
            return state;
        }

        for(int i = 0, j = 0; i < temp.row_size && j < matrix1->col_size; i++, j++){
            *det *= temp.value[i][j];
        }

        state = mtxdel(&temp);

        if( state != SUCCESS){
            if(state == NULL_POINTER)
                return NULL_POINTER;
            return CANNOT_DELETE_POINTER;
        }
    }
    return SUCCESS;
}


err_code mtxrf(matrix *matrix1, char *str){

    FILE *f_ptr;
    f_ptr = fopen(str, "r");
    if(f_ptr == NULL)
        return FILE_NOT_FOUND;

    err_code state = mtxdel(matrix1);

    if( state != SUCCESS){
        fclose(f_ptr);
        if(state == NULL_POINTER)
            return NULL_POINTER;
        return CANNOT_DELETE_POINTER;
    }

    fscanf(f_ptr, "%d %d", &matrix1->row_size, &matrix1->col_size);
    if(mtxinit(matrix1, matrix1->row_size, matrix1->col_size) != SUCCESS)
        return INCOMPATIBLE_TYPES;

    for(int i = 0; i < matrix1->row_size; i++){
        for(int j = 0; j < matrix1->col_size; j++){
            fscanf(f_ptr, "%lf", &matrix1->value[i][j]);
        }
    }

    fclose(f_ptr);
    return SUCCESS;
}


err_code mtxwf(matrix matrix1, char *str){

    if(null_pointer_check(&matrix1))
        return NULL_POINTER;

    FILE *f_ptr;
    f_ptr = fopen(str, "w");

    fprintf(f_ptr, "%d %d\n", matrix1.row_size, matrix1.col_size);
    for(int i = 0; i < matrix1.row_size; i++){
        for(int j = 0; j < matrix1.col_size; j++){
            fprintf(f_ptr, "%.2f\t", matrix1.value[i][j]);
        }
        fprintf(f_ptr,"\n");
    }

    fclose(f_ptr);
    return SUCCESS;
}


err_code mtxprint(matrix matrix1){

    if(null_pointer_check(&matrix1))
        return NULL_POINTER;

    printf("\n");
    for(int i = 0; i < matrix1.row_size; i++){
        if(i == 0){
            for (int j = 0; j < matrix1.col_size; ++j) {
                printf("\t[%d]:", j+1);
            }
        }
        printf("\n[%d]: ", i+1);
        for (int j = 0; j < matrix1.col_size; ++j) {
            printf("\t %.2f", matrix1.value[i][j]);
        }
    }
    printf("\n");
    return SUCCESS;
}


err_code mtxdel(matrix *matrix1){

    if(null_pointer_check(matrix1))
        return NULL_POINTER;

    for(int i = 0; i < matrix1->row_size; i++){
        free(matrix1->value[i]);
    }
    free(matrix1->value);
    return SUCCESS;
}




static void switch_row(matrix *matrix1, int row1, int row2){
    double temp;
    for (int i = 0; i < matrix1->col_size; ++i) {
        temp = matrix1->value[row1][i];
        matrix1->value[row1][i] = matrix1->value[row2][i];
        matrix1->value[row2][i] = temp;
    }
}

static void check_row(matrix *matrix1,int indi,int *indj, double *coef){
    if(matrix1->value[indi][*indj] == 0){
        for(int j = *indj; j < matrix1->col_size; j++){
            for(int i = indi; i < matrix1->row_size; i++){
                if(matrix1->value[i][j] != 0){
                    *coef *= -1;
                    switch_row(matrix1, i, indi);
                    *indj = j;
                    return;
                }
            }
        }
    }
}

static void sub_row(matrix *matrix1,int indi, int indj){
    for(int i = indi+1; i < matrix1->row_size; i++){
        for(int j = matrix1->col_size-1; j >=indj ; j--){
            matrix1->value[i][j] -= matrix1->value[indi][j]*matrix1->value[i][indj];
        }
    }
    for(int i = 0; i < indi; i++){
        for(int j = matrix1->col_size-1; j >=indj ; j--){
            matrix1->value[i][j] -= matrix1->value[indi][j]*matrix1->value[i][indj];
        }
    }
}

static void aprox(matrix *matrix1){
    for(int i = 0; i < matrix1->row_size; i++){
        for(int j = 0; j < matrix1->col_size; j++){
            matrix1->value[i][j] = ceil(matrix1->value[i][j]*10000000000000)/10000000000000;
            if(fabs(matrix1->value[i][j])< 0.000001){
                matrix1->value[i][j] = 0.0;
            }
        }
    }
}

static err_code Gauss(matrix *matrix1, double *coef){
    *coef = 1;
    if(null_pointer_check(matrix1))
        return NULL_POINTER;

    for (int i = 0, j = 0; i < matrix1->row_size && j <  matrix1->col_size; ++i, ++j) {
        check_row(matrix1, i, &j, coef);

        *coef *= matrix1->value[i][j];
        double temp = matrix1->value[i][j];

        if(temp!=0){
            for(int k = 0; k < matrix1->col_size; k++)
                matrix1->value[i][k] /= temp;
        }

        sub_row(matrix1, i, j);
    }
    aprox(matrix1);

    return SUCCESS;
}

static void mtxinv_init(matrix *matrix1,matrix *matrix2){
    mtxzinit(matrix2, matrix1->row_size, 2*matrix1->col_size);
    for(int i = 0; i < matrix1->row_size; i++){
        for (int j = 0; j < matrix1->col_size; j++) {
            matrix2->value[i][j] = matrix1->value[i][j];
        }
    }
    for (int i = matrix1->col_size; i < matrix2->col_size; i++) {
        matrix2->value[i-matrix1->col_size][i] = 1;
    }
}

static bool inv_det_check(matrix matrix1){
    for(int i = 0; i < matrix1.row_size; i++){
            if(matrix1.value[i][i] == 0)
                return false;
    }
    return true;
}

static void mtxinv_sep(matrix *matrix1, matrix *matrix2){
    for (int i = 0; i < matrix1->row_size; i++) {
        for (int j = matrix2->col_size; j < matrix1->col_size; j++) {
            matrix2->value[i][j-matrix2->col_size] = matrix1->value[i][j];

        }
    }
}

static bool null_pointer_check(const matrix* matrix1){
    if(matrix1 == NULL)
        return true;
    if (matrix1->value == NULL)
        return true;
    for (int i = 0; i < matrix1->row_size; ++i) {
        if (matrix1->value[i] == NULL)
            return true;
    }
    return false;
}